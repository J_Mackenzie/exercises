﻿using System;

namespace demo_02
{
    class Program
    {
        public static void PrintGreeting(string name)
        {
            Console.WriteLine(name);
        }
        public static void Main(String[] args)
        {
            PrintGreeting("My First Method");
            PrintGreeting("My First Method");
            PrintGreeting("My First Method");
            PrintGreeting("My First Method");
            PrintGreeting("My First Method");
        }
    }
}
